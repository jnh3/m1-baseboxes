packer {
  required_plugins {
    parallels = {
      version = ">= 1.0.0"
      source  = "github.com/hashicorp/parallels"
    }
  }
}

variables {
  basebox_name     = "debian-11.7-arm64"
  build_directory  = "../builds"
  iso_name         = "debian-11.7.0-arm64-netinst.iso"
  iso_checksum     = "174caba674fe3172938439257156b9cb8940bb5fd5ddf124256e81ec00ec460d"
  mirror           = "https://cdimage.debian.org/cdimage/archive"
  mirror_directory = "11.7.0/arm64/iso-cd"
  preseed_path     = "debian-9/preseed.cfg"
}

locals {
  http_directory = "${path.root}/http"
}

source "parallels-iso" "debian-11-7-arm64" {
  boot_command = [
    "e<wait>",
    "<down><down><down><right><right><right><right><right><right><right><right><right><right>",
    "<right><right><right><right><right><right><right><right><right><right><right><right><right>",
    "<right><right><right><right><right><right><right><right><right><right><right><wait>",
    "install <wait>",
    " preseed/url=http://{{ .HTTPIP }}:{{ .HTTPPort }}/${var.preseed_path} <wait>",
    "debian-installer=en_US.UTF-8 <wait>",
    "auto <wait>",
    "locale=en_US.UTF-8 <wait>",
    "kbd-chooser/method=us <wait>",
    "keyboard-configuration/xkb-keymap=us <wait>",
    "netcfg/get_hostname=${var.basebox_name} <wait>",
    "netcfg/get_domain=vagrantup.com <wait>",
    "fb=false <wait>",
    "debconf/frontend=noninteractive <wait>",
    "console-setup/ask_detect=false <wait>",
    "console-keymaps-at/keymap=us <wait>",
    "grub-installer/bootdev=/dev/sda <wait>",
    "<f10><wait>"
  ]
  boot_wait              = "5s"
  cpus                   = "2"
  disk_size              = "65536"
  guest_os_type          = "debian"
  http_directory         = "${local.http_directory}"
  iso_checksum           = "${var.iso_checksum}"
  iso_url                = "${var.mirror}/${var.mirror_directory}/${var.iso_name}"
  memory                 = "1024"
  output_directory       = "${var.build_directory}/packer-${var.basebox_name}-parallels"
  parallels_tools_flavor = "lin-arm"
  prlctl_version_file    = ".prlctl_version"
  shutdown_command       = "echo 'vagrant' | sudo -S shutdown -hP now"
  ssh_password           = "vagrant"
  ssh_port               = 22
  ssh_timeout            = "10000s"
  ssh_username           = "vagrant"
  vm_name                = "${var.basebox_name}"
}

build {
  sources = ["source.parallels-iso.debian-11-7-arm64"]

  provisioner "shell" {
    environment_vars  = ["HOME_DIR=/home/vagrant"]
    execute_command   = "echo 'vagrant' | {{ .Vars }} sudo -S -E sh -eux '{{ .Path }}'"
    expect_disconnect = true
    scripts = [
      "${path.root}/scripts/update.sh",
      "${path.root}/../_common/motd.sh",
      "${path.root}/../_common/sshd.sh",
      "${path.root}/scripts/networking.sh",
      "${path.root}/scripts/sudoers.sh",
      "${path.root}/../_common/vagrant.sh",
      "${path.root}/scripts/systemd.sh",
      "${path.root}/../_common/parallels.sh",
      "${path.root}/scripts/cleanup.sh",
      "${path.root}/../_common/minimize.sh"
    ]
  }


  post-processor "vagrant" {
    output = "${var.build_directory}/${var.basebox_name}.box"
  }
}
